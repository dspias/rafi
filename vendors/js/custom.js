$(document).ready(function(){
    $('select:not(.ignore)').niceSelect();

    let shaft_style = $('#shaft_style');

    calculate();

    $(shaft_style).on('change',function(){
        if (shaft_style.val() == 0) {
            $('#ri').removeAttr('disabled');
        } else {
            $('#ri').attr('disabled', 'disabled');
        }
    });

    $('.calculate').on('input', function(){
        calculate();
    });

    $('.dcalculate').change(function(){
        calculate();
    });
    $('#sumbit').click(function(){
        calculate();
    });


    function calculate(){
        // user input perameter
        let t = $('#t');
        let t_unit = $('#t_unit');
    
        let w = $('#w');
    
        let r0 = $('#r0');
        let ri = $('#ri');
        let l = $('#l');
        let r0ril_unit = $('#r0ril_unit');
    
        let thita = $('#thita');
        let thita_unit = $('#thita_unit');
    
        // output perameter
        let tmax = $('#tmax');
        let tmax_unit = $('#tmax_unit');
    
        let g = $('#g');
        let g_unit = $('#g_unit');
    
        let p = $('#p');
        let p_unit = $('#p_unit');
    
        let j = $('#j');
        let j_unit = $('#j_unit');


        // calculation here
        let t_val, t_unit_val, w_val, r0_val, ri_val, l_val, r0ril_unit_val, thita_val, thita_unit_val, tmax_unit_val, g_unit_val, p_unit_val, j_unit_val, tmax_val, g_val, p_val, j_val;
        const pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679821480865132823066470938446095505822317253594081284811174502841027019385211055596446229489549303819644288109756659334461284756482337867831652712019091456485669234603486104543266482133936072602491412737245870066063155881748815209209628292540917153643678925903600113305305488204665213841469519415116094330572703657595919530921861173819326117931051185480744623799627495673518857527248912279381830119491298336733624406566430860213949463952247371907021798609437027705392171762931767523846748184676694051320005681271452635608277857713427577896091736371787214684409012249534301465495853710507922796892589235420199561121290219608640344181598136297747713099605187072113499999983729780499510597317328160963185950244594553469083026425223082533446850352619311881710100031378387528865875332083814206171776691473035982534904287554687311595628638823537875937519577818577805321712268066130019278766111959092164201989;

        t_val = Number(t.val());
        t_unit_val = Number(t_unit.val());
        w_val = Number(w.val());
        r0_val = Number(r0.val()); 
        ri_val = Number(ri.val());
        l_val = Number(l.val());
        r0ril_unit_val = Number(r0ril_unit.val());
        thita_val = Number(thita.val());
        thita_unit_val = Number(thita_unit.val());
        tmax_unit_val = Number(tmax_unit.val());
        g_unit_val = Number(g_unit.val());
        p_unit_val = Number(p_unit.val());
        j_unit_val = Number(j_unit.val());

        
        // calculate J value
        r0_val = r0_val*r0ril_unit_val;
        ri_val = ri_val*r0ril_unit_val;

        j_val = ($('#shaft_style').val() == 1) ? ( (pi/2) * Math.pow(r0_val, 4) ) : ( (pi/2) * ( Math.pow(r0_val, 4) - Math.pow(ri_val, 4) ) );



        // calculate tmax value
        tmax_val = ( (t_val * t_unit_val) * r0_val ) / j_val;



        // calculate P value
        p_val =(t_val * t_unit_val) * pi * ( w_val/30) ;



        // calculate G value
        g_val =((t_val * t_unit_val) * ( l_val * r0ril_unit_val))/ (j_val * ( thita_val * thita_unit_val)) ;


        // print value j value
        if(j_unit_val == 1){
            j.val(Math.floor(j_val/j_unit_val));
        } else{
            j.val((j_val/j_unit_val).toFixed(3));
        }

        // print value t_max value
        tmax.val((tmax_val/tmax_unit_val).toFixed(3));

        // print value P value
        p.val((p_val/p_unit_val).toFixed(3));

        // print value G value
        g.val((g_val/g_unit_val).toFixed(3));
    }






















});